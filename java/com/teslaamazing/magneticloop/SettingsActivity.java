package com.teslaamazing.magneticloop;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.teslaamazing.magneticloop.utils.FileChooser;

public class SettingsActivity extends Activity {

    private static final int CHOOSE_FILE_REQUEST_CODE = 0;

    private Preferences prefs;
    private EditText mServerUrlView;
    private EditText mVideoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mServerUrlView = (EditText) findViewById(R.id.server_url);
        mVideoFile = (EditText) findViewById(R.id.video_file);
        prefs = Preferences.from(this);

        Button mSaveButton = (Button) findViewById(R.id.bSave);
        assert mSaveButton != null;
        mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
                finish();
            }
        });

        Button mCancelButton = (Button) findViewById(R.id.bCancel);
        assert mCancelButton != null;
        mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOOSE_FILE_REQUEST_CODE:
                    if (resultData != null) {
                        String file_path = resultData.getStringExtra("path");
                        if (file_path != null) {
                            prefs.put(Preferences.Key.VIDEO_FILE, file_path).apply();
                        }
                    }
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mServerUrlView.setText(prefs.getString(Preferences.Key.SERVER_URL, Constants.SERVER_URL));
        mVideoFile.setText(prefs.getString(Preferences.Key.VIDEO_FILE));
    }

    /**
     * Save settings
     */
    private void save() {
        prefs.put(Preferences.Key.SERVER_URL, mServerUrlView.getText().toString());
        prefs.put(Preferences.Key.VIDEO_FILE, mVideoFile.getText().toString());
        prefs.apply();
    }

    /**
     * Get file
     */
    public void getFile(View view) {
        Intent intent = new Intent(this, FileChooser.class);
        startActivityForResult(intent, CHOOSE_FILE_REQUEST_CODE);
    }
}

