package com.teslaamazing.magneticloop;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Preferences {

    public static final String NAME = "settings";
    public static final int MODE = Context.MODE_PRIVATE;

    public static class Key {
        public static final String SERVER_URL = "server_url";
        public static final String SERVER_INTERVAL = "server_interval";
        public static final String VIDEO_FILE = "video_file";
        public static final String STAT = "stat";
        public static final String WIFI = "wifi";
        public static final String PHONE = "phone";
        public static final String BALANCE = "balance";
        public static final String TEST_MOBILE = "test_mobile_data";
    }

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Context context;

    private Preferences(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(NAME, MODE);
    }

    public static Preferences from(Context context) {
        return new Preferences(context);
    }

    public SharedPreferences.Editor put(String key, String val) {
        return editor().putString(key, val);
    }

    public SharedPreferences.Editor put(String key, int val) {
        return editor().putInt(key, val);
    }

    public SharedPreferences.Editor put(String key, boolean val) {
        return editor().putBoolean(key, val);
    }

    public SharedPreferences.Editor put(String key, float val) {
        return editor().putFloat(key, val);
    }

    public SharedPreferences.Editor put(String key, double val) {
        return editor().putString(key, String.valueOf(val));
    }

    public SharedPreferences.Editor put(String key, long val) {
        return editor().putLong(key, val);
    }

    @Nullable
    public String getString(String key, String defaultValue) {
        return prefs.getString(key, defaultValue);
    }

    @Nullable
    public String getString(String key) {
        return prefs.getString(key, null);
    }

    public int getInt(String key) {
        return prefs.getInt(key, 0);
    }

    public int getInt(String key, int defaultValue) {
        return prefs.getInt(key, defaultValue);
    }

    public long getLong(String key) {
        return prefs.getLong(key, 0);
    }

    public long getLong(String key, long defaultValue) {
        return prefs.getLong(key, defaultValue);
    }

    public float getFloat(String key) {
        return prefs.getFloat(key, 0);
    }

    public float getFloat(String key, float defaultValue) {
        return prefs.getFloat(key, defaultValue);
    }

    /**
     * Convenience method for retrieving doubles.
     * <p/>
     * There may be instances where the accuracy of a double is desired.
     * SharedPreferences does not handle doubles so they have to
     * cast to and from String.
     *
     * @param key The name of the preference to fetch.
     */
    public double getDouble(String key) {
        return getDouble(key, 0);
    }

    /**
     * Convenience method for retrieving doubles.
     * <p/>
     * There may be instances where the accuracy of a double is desired.
     * SharedPreferences does not handle doubles so they have to
     * cast to and from String.
     *
     * @param key The name of the preference to fetch.
     */
    public double getDouble(String key, double defaultValue) {
        try {
            return Double.valueOf(prefs.getString(key, String.valueOf(defaultValue)));
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return prefs.getBoolean(key, defaultValue);
    }

    public boolean getBoolean(String key) {
        return prefs.getBoolean(key, false);
    }

    public SharedPreferences.Editor editor() {
        if (editor == null) {
            editor = prefs.edit();
        }
        return editor;
    }

    public void apply() {
        if (editor != null) {
            editor.apply();
        }
    }

    /**
     * Remove keys from SharedPreferences.
     *
     * @param keys The name of the key(s) to be removed.
     */
    public void remove(String... keys) {
        for (String key : keys) {
            editor().remove(key);
        }
        apply();
    }

    /**
     * Remove all keys from SharedPreferences.
     */
    public void clear() {
        editor().clear().apply();
    }


    public String getServerUrl() {
        return getString(Key.SERVER_URL, Constants.SERVER_URL);
    }

    public int getServerInterval() {
        return getInt(Key.SERVER_INTERVAL, Constants.SERVER_INTERVAL);
    }

    public String getVideoFile() {
        return getString(Key.VIDEO_FILE);
    }

    public int getStat() {
        return getInt(Key.STAT);
    }

    public void incrementStat() {
        put(Key.STAT, getStat() + 1).apply();
    }

    public WifiConfiguration getWifiConfiguration() throws JSONException {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        JSONObject json = new JSONObject(prefs.getString(Key.WIFI, "{}"));
        if (json.has("ssid") && json.has("pass")) {
            wifiConfig.SSID = String.format("\"%s\"", json.get("ssid"));
            wifiConfig.preSharedKey = String.format("\"%s\"", json.get("pass"));
        }
        return wifiConfig;
    }

    public void sync(JSONObject data) {
        try {
            if (data.length() > 0) {
                SharedPreferences.Editor editor = prefs.edit();
                Iterator<?> keys = data.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    switch (key) {
                        case Key.VIDEO_FILE:
                        case Key.STAT:
                            // Do not update directly
                            break;
                        case Key.WIFI:
                            // json object
                            editor.putString(key, data.getJSONObject(key).toString());
                            break;
                        case "stat_reset":
                            editor.putInt(Key.STAT, 0);
                            break;
                        default:
                            editor.putString(key, data.getString(key));
                    }
                }
                editor.apply();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
