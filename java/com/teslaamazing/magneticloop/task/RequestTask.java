package com.teslaamazing.magneticloop.task;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Form;
import com.afollestad.bridge.Response;
import com.teslaamazing.magneticloop.App;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

public class RequestTask extends AsyncTask<String, Void, String> {

    public interface EventListener {
        void onPreExecute(Form form);

        void onPostExecute(JSONObject result) throws JSONException;
    }

    private EventListener listener;
    private Form form;

    public RequestTask(EventListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        form = new Form()
                .add("sdk", android.os.Build.VERSION.SDK_INT)
                .add("ts", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(Calendar.getInstance().getTime()))
        ;
        listener.onPreExecute(form);
    }

    @Override
    protected String doInBackground(String... params) {
//        return "{video: \"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4\"}";
//        return "{video: \"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4\"}";
//        return "{video_url: \"https://teslaamazing.com/media/display_video/13/2012-07-07_17.27.34.mp4)\"}";
//        return "{video: \"https://teslaamazing.com/media/display_video/default.mp4\"}";
//        return "{device_id: \"000000000000000\"}";
//        return "{update: { url: \"https://teslaamazing.com/media/apk/app-release.apk\" }}";
        if (params.length < 1) {
            return null;
        }
        String serverUrl = params[0];
        if (TextUtils.isEmpty(serverUrl)) {
            return null;
        }
        try {
            App.log("Request: " + serverUrl + "?" + form.toString());
            Response response = Bridge.post(serverUrl).body(form).throwIfNotSuccess().response();
            if (response != null) {
                return response.asString();
            }
        } catch (BridgeException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        App.log("Response: " + result);
        if (result != null && result.length() > 0 && !Objects.equals(result, "[]")) {
            try {
                listener.onPostExecute(new JSONObject(result));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}