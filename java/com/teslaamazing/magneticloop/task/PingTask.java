package com.teslaamazing.magneticloop.task;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class PingTask extends AsyncTask<String, Void, Boolean> {

    public interface EventListener {
        void onPostExecute(boolean result);
    }

    private EventListener listener;

    public PingTask(EventListener listener) {
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        Socket socket = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress("google.com", 80), 2000);
            return socket.isConnected();
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        listener.onPostExecute(result);
    }
}
