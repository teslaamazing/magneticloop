package com.teslaamazing.magneticloop.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import eu.chainfire.libsuperuser.Shell;

final public class NetworkUtil {

    public static NetworkInfo getActiveNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected(Context context) {
        NetworkInfo activeNetwork = getActiveNetwork(context);
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public static boolean isConnected(Context context, int type) {
        NetworkInfo activeNetwork = getActiveNetwork(context);
        return activeNetwork != null && activeNetwork.getType() == type && activeNetwork.isConnected();
    }

    /**
     * Class Mobile
     */
    public static class Mobile {

        public static boolean isConnected(Context context) {
            return NetworkUtil.isConnected(context, ConnectivityManager.TYPE_MOBILE);
        }

        public static void setEnabled(Context context, boolean enabled) {
//            Shell.SU.run("settings put global mobile_data " + enabled);
            try {
                int state = enabled ? 1 : 0;
                final TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                final Class<?> mTelephonyClass = Class.forName(mTelephonyManager.getClass().getName());
                final Method mTelephonyMethod = mTelephonyClass.getDeclaredMethod("getITelephony");
                mTelephonyMethod.setAccessible(true);
                final Object mTelephonyStub = mTelephonyMethod.invoke(mTelephonyManager);
                final Class<?> mTelephonyStubClass = Class.forName(mTelephonyStub.getClass().getName());
                final Class<?> mClass = mTelephonyStubClass.getDeclaringClass();
                final Field field = mClass.getDeclaredField("TRANSACTION_setDataEnabled");
                field.setAccessible(true);
                String transactionCode = String.valueOf(field.getInt(null));
                if (transactionCode.length() > 0) {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                        SubscriptionManager mSubscriptionManager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                        // Loop through the subscription list i.e. SIM list.
                        for (int i = 0; i < mSubscriptionManager.getActiveSubscriptionInfoCountMax(); i++) {
                            // Get the active subscription ID for a given SIM card.
                            int subscriptionId = mSubscriptionManager.getActiveSubscriptionInfoList().get(i).getSubscriptionId();
                            // Execute the command via `su` to turn off
                            // mobile network for a subscription service.
                            Shell.SU.run("service call phone " + transactionCode + " i32 " + subscriptionId + " i32 " + state);
                        }
                    } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                        // Android 5.0 (API 21) only.
                        Shell.SU.run("service call phone " + transactionCode + " i32 " + state);
                    } else {
                        Shell.SU.run("svc data " + (enabled ? "enable" : "disable"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Class Wifi
     */
    public static class Wifi {

        public enum Capability {
            SECURITY_WEP,
            SECURITY_WPA,
            SECURITY_WPA2,
            SECURITY_EAP,
            SECURITY_NONE
        }

        public static boolean isConnected(Context context) {
            return NetworkUtil.isConnected(context, ConnectivityManager.TYPE_WIFI);
        }

        public static boolean isEnabled(Context context) {
            return ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).isWifiEnabled();
        }

        public static void setEnabled(Context context, boolean enabled) {
            ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).setWifiEnabled(enabled);
        }

        public static WifiConfiguration getOpenConfiguration(String ssid) {
            if (ssid == null || ssid.trim().isEmpty()) {
                return null;
            }
            WifiConfiguration wifiConfiguration = new WifiConfiguration();
            wifiConfiguration.SSID = ssid;
            wifiConfiguration.allowedKeyManagement.set(0);
            return wifiConfiguration;
        }

        public static int connect(Context context, WifiConfiguration wifiConfig) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            int netId = wifiManager.addNetwork(wifiConfig);
            wifiManager.enableNetwork(netId, true);
            wifiManager.reconnect();
            return netId;
        }

        public static int connect(Context context, String ssid) {
            return connect(context, getOpenConfiguration(ssid));
        }

        public static Capability getSecurity(String capabilities) {
            Capability capability = Capability.SECURITY_NONE;
            if (capabilities.contains("WEP")) {
                return Capability.SECURITY_WEP;
            }
            if (capabilities.contains("WPA2")) {
                return Capability.SECURITY_WPA2;
            }
            if (capabilities.contains("WPA")) {
                return Capability.SECURITY_WPA;
            }
            if (capabilities.contains("EAP")) {
                return Capability.SECURITY_EAP;
            }
            return capability;
        }
    }
}
