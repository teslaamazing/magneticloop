package com.teslaamazing.magneticloop.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Callback;
import com.afollestad.bridge.Request;
import com.afollestad.bridge.Response;
import com.getsentry.raven.Raven;
import com.teslaamazing.magneticloop.App;
import com.teslaamazing.magneticloop.Constants;
import com.teslaamazing.magneticloop.Preferences;
import com.teslaamazing.magneticloop.USSDService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import eu.chainfire.libsuperuser.Shell;

final public class AppUtil {

    /**
     * Connect to wifi with predefined networks
     *
     * @param wifiManager
     */
    public static void configureWifi(WifiManager wifiManager) {
        WifiConfiguration wifiConfig;

        // Service network
        wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", "service");
        wifiConfig.preSharedKey = String.format("\"%s\"", "12345678");
        wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfig), false);

        // Production network
        wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", "Elion_41AD");
        wifiConfig.preSharedKey = String.format("\"%s\"", "AD4353BB1F6C3");
        wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfig), false);

        // Alex Home network
        wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", "A&N");
        wifiConfig.preSharedKey = String.format("\"%s\"", "ab20031985");
        wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfig), false);

        // Home network
        wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", "FBI");
        wifiConfig.preSharedKey = String.format("\"%s\"", "86767248");
        wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfig), false);

//        wifiManager.reassociate();

        if (wifiManager.isWifiEnabled()) {
            wifiManager.reconnect();
        }
    }

    /**
     * Set new wallpaper
     *
     * @param path to image file
     */
    public static void updateWallpaper(Context context, String path) {
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
        File f = new File(path);
        if (f.exists()) {
            try {

                FileInputStream is = new FileInputStream(f);
                BufferedInputStream bis = new BufferedInputStream(is);

                // Fit image to fullsceen
                DisplayMetrics metrics = new DisplayMetrics();
                WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                windowManager.getDefaultDisplay().getMetrics(metrics);

                Bitmap bitmap = BitmapFactory.decodeStream(bis);

                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {

                    float scale = (float) 0.66;

                    Bitmap background = Bitmap.createBitmap(metrics.widthPixels, metrics.heightPixels, Bitmap.Config.ARGB_8888);

                    float originalWidth = bitmap.getWidth(), originalHeight = bitmap.getHeight();

                    Canvas canvas = new Canvas(background);
                    float xTranslation = 0.0f, yTranslation = 0.0f;

                    if (originalWidth > originalHeight) {
//                    scale = metrics.heightPixels / originalHeight;
                        xTranslation = (metrics.widthPixels - originalWidth * scale) / 2.0f;
                    } else {
//                    scale = metrics.widthPixels / originalWidth;
                        yTranslation = (metrics.heightPixels - originalHeight * scale) / 2.0f;
                    }

                    Matrix transformation = new Matrix();
                    transformation.postTranslate(xTranslation - 100, yTranslation + 80);
                    transformation.preScale(scale, scale);

                    Paint paint = new Paint();
                    paint.setFilterBitmap(true);
                    canvas.drawBitmap(bitmap, transformation, paint);

                    wallpaperManager.setBitmap(background);
//                    Log.d("@@@@", "Metrics " + metrics.toString());
//                    Log.d("@@@@", "Bitmap DPI" + bitmap.getDensity());
//                    Log.d("@@@@", "originalWidth: " + originalWidth);
//                    Log.d("@@@@", "originalHeight: " + originalHeight);
//                    Log.d("@@@@", "xTranslation: " + xTranslation);
//                    Log.d("@@@@", "yTranslation: " + yTranslation);
//                    Log.d("@@@@", "scale: " + scale);
//                    Log.d("@@@@", "getDesiredMinimumWidth: " + wallpaperManager.getDesiredMinimumWidth());
//                    Log.d("@@@@", "getDesiredMinimumHeight: " + wallpaperManager.getDesiredMinimumHeight());

                } else {
                    wallpaperManager.setBitmap(bitmap);
                }

                bis.close();
                is.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Update APN
     *
     * @param context
     */
    public static void updateAPN(Context context) {

        String id = "9999";

        // TODO: dynamic
//        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        String numeric = tel.getSimOperator();
//        // Sim not exists
//        if (Objects.equals(numeric, "")) {
//            numeric = "00000";
//        }

        String numeric = "24801";

        Shell.SU.run(new String[]{
                "content delete --uri " + Constants.TELEPHONY_CARRIERS_URI + " --where '_id=" + id + "'",
                "content insert --uri " + Constants.TELEPHONY_CARRIERS_URI +
                        " --bind _id:i:" + id +
                        " --bind name:s:" + Constants.APN_NAME +
                        " --bind apn:s:" + Constants.APN_URL +
                        " --bind mcc:s:" + numeric.substring(0, 3) +
                        " --bind mnc:s:" + numeric.substring(3) +
                        " --bind numeric:s:" + numeric,
                "content insert --uri " + Constants.TELEPHONY_CARRIERS_URI + "/preferapn --bind apn_id:i:" + id
        });
    }

    /**
     * Set new IMEI for SIM1 and restart device
     *
     * @param deviceId New device identifier
     */
    public static void updateIMEI(String deviceId) {
        Shell.SU.run("echo 'AT +EGMR=1,7,\"" + deviceId + "\"'> " + Constants.RIL_CHANNEL);
    }

    /**
     * Remove all shortcuts from favorites
     */
    public static void cleanLauncherFavorites() {
//        Shell.SU.run("content delete --uri " + Constants.LAUNCHER_FAVORITES_URI + " --where 'intent IS NOT null'"); // with clock
        Shell.SU.run("content delete --uri " + Constants.LAUNCHER_FAVORITES_URI); // without clock
    }

    /**
     * Remove unused files
     */
    public static void cleanFiles() {
        File dir = new File(Environment.getExternalStorageDirectory() + "/Download");
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String name : children) {
                File file = new File(dir, name);
                file.delete();
//                String ext = getFileExtension(file);
//                if (!Objects.equals(ext, "mp4")) {
//                    file.delete();
//                }
            }
        }
    }

    /**
     * Get file extension
     */
    public static String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Disable All notifications
     */
    public static void disableNotifications(Context context) {
        try {
            Field field = Class.forName("android.app.INotificationManager").getDeclaredClasses()[0].getDeclaredField("TRANSACTION_setNotificationsEnabledForPackage");
            field.setAccessible(true);
            Integer command = field.getInt(null);
            final PackageManager pm = context.getPackageManager();
            List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
            if (packages.size() > 0) {
                List<String> commands = new ArrayList<String>();
                for (ApplicationInfo app : packages) {
                    if (!Objects.equals(app.packageName, context.getPackageName())) {
                        String cmd = String.format(Locale.getDefault(),
                                "service call notification %d s16 %s i32 %d i32 %d",
                                command,
                                app.packageName,
                                app.uid,
                                0 // disabled
                        );
                        commands.add(cmd);
                    }
                }
                Shell.SU.run(commands);
            }
        } catch (NoSuchFieldException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Restrict background data usage
     */
    public static void restrictBackgroundDataUsage() {
        try {
            Field field = Class.forName("android.net.INetworkPolicyManager").getDeclaredClasses()[0].getDeclaredField("TRANSACTION_setRestrictBackground");
            field.setAccessible(true);
            Integer command = field.getInt(null);
            Shell.SU.run(String.format(Locale.getDefault(), "service call netpolicy %d i32 %d", command, 1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param serviceName The service name to be included.
     */
    public static void enableAccessabilityService(Context context, String serviceName) {
        String services = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (TextUtils.isEmpty(services)) {
            services = serviceName;
        } else {
            services += ":" + serviceName;
        }
        Shell.SU.run(new String[]{
                String.format("settings put secure %s %s", Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES, services),
                String.format("settings put secure %s %s", Settings.Secure.ACCESSIBILITY_ENABLED, 1),
        });
    }

    public static boolean isAccessibilitySettingsOn(Context context) {
        int accessibilityEnabled = 0;
        try {
            accessibilityEnabled = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if (accessibilityEnabled == 1) {
            String services = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (services != null) {
                return services.toLowerCase().contains(context.getPackageName().toLowerCase());
            }
        }
        return false;
    }

    /**
     * @param input String
     * @return Encoded string
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
        byte[] buffer = input.getBytes("UTF-8");
        md.update(buffer);
        byte[] digest = md.digest();
        String hexStr = "";
        for (byte b : digest) {
            hexStr += Integer.toString((b & 0xff) + 0x100, 16).substring(1);
        }
        return hexStr;
    }

    /**
     * @param activity
     */
    @UiThread
    public static void root(final Activity activity) {

        final String path = Environment.getExternalStorageDirectory().toString() + "/Download/root.apk";
        String tag = "root";

        final ProgressDialog pDialog = new ProgressDialog(activity);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setTitle("Magnetic Loop");
        pDialog.setMessage("Rooting... Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setMax(100);
        pDialog.show();

        Bridge.cancelAll().tag(tag).commit();
        Bridge.get(Constants.ROOT_APK_URL).tag(tag).retries(5, 2000).request(new Callback() {

            @Override
            public void response(@NonNull Request request, @Nullable Response response, @Nullable BridgeException e) {
                try {
                    File file = new File(path);
                    if (response != null) {
                        response.asFile(file);
                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                } catch (BridgeException e1) {
                    e1.printStackTrace();
                } finally {
                    pDialog.hide();
                }
            }

            @Override
            public void progress(Request request, int current, int total, int percent) {
                super.progress(request, current, total, percent);
                pDialog.setProgress(percent);
                App.log(String.format("progress: %s", percent));
            }
        });
    }

    @UiThread
    public static void setup(Context context, File file, String deviceId) {
        try {
            String dirPath = file.getParent();
            ZipUtil.unzip(file.getPath(), dirPath);
//
            // Install sqlite3
            File sqlite = new File(dirPath, "sqlite3");
            if (sqlite.exists()) {
                Shell.SU.run(new String[]{
                        "mount -o rw,remount /system",
                        "cp " + sqlite.getPath() + " /system/xbin/",
                        "chmod 0755 /system/xbin/sqlite3",
                });
            }

            // Set wallpaper image
            File wallpaper = new File(dirPath, "wallpaper.png");
            if (wallpaper.exists()) {
                AppUtil.updateWallpaper(context, wallpaper.getPath());
            }

            List<String> commands = new ArrayList<>();

            commands.add(String.format("settings put global %s %s", Settings.Global.ADB_ENABLED, 1));
            commands.add(String.format("settings put system %s %s", Settings.System.SCREEN_BRIGHTNESS, 204));

            // disable google verification
            commands.add(String.format("settings put global %s %s", "upload_apk_enable", 0));
            commands.add(String.format("settings put global %s %s", "package_verifier_enable", 0));

            // Immersive mode
            commands.add(String.format("settings put global policy_control immersive.full=%s", context.getPackageName()));
            commands.add(String.format("settings put secure %s %s", "immersive_mode_confirmations", "confirmed"));

            // Mobile data
            // TODO: suffix
            commands.add(String.format("settings put global %s %s", "mobile_data", 1));

            // Data roaming
            // TODO: suffix
            commands.add(String.format("settings put system %s %s", "roaming_reminder_mode_setting", 2));
            commands.add(String.format("content update --uri %s --bind data_roaming:s:%s", Constants.TELEPHONY_SIMINFO_URI, 1));
            commands.add(String.format("settings put global %s %s", Settings.Global.DATA_ROAMING, 1));
//                    commands.add(String.format("settings put global %s %s", Settings.Global.DATA_ROAMING + "1", 1)); // 1 sim
//                    commands.add(String.format("settings put global %s %s", Settings.Global.DATA_ROAMING + "2", 1)); // 2 sim
//                    commands.add(String.format("settings put global %s %s", Settings.Global.DATA_ROAMING + "3", 1)); // 3 sim

            // Lock settings
            // TODO: remove locksettings.db
            commands.add(String.format("settings put secure %s %s", "lockscreen.disabled", 1));
            commands.add(String.format("settings put secure %s %s", "lockscreen.options", ""));
            // TODO: remove sqlite3
            commands.add("sqlite3 " + Constants.LOCKSETTINGS_DB + " \"UPDATE locksettings SET value = '1' WHERE name = 'lockscreen.disabled'\"");

            // Audio settings
            commands.add(String.format("settings put system %s %s", "volume_music_speaker", 0));
            commands.add(String.format("settings put system %s %s", "volume_ring_speaker", 0));
            commands.add(String.format("settings put system %s %s", "mtk_audioprofile_general_volume_ring", 0));
            commands.add(String.format("settings put system %s %s", "mtk_audioprofile_general_volume_notification", 0));

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                commands.add(String.format("settings put secure %s %s", "battery_percentage", 1));
                commands.add(String.format("settings put system %s %s", "volume_alarm_speaker", 0));
            } else {
                commands.add(String.format("settings put system %s %s", Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL));
                commands.add(String.format("settings put system %s %s", "background_power_saving_enable", 1));
                commands.add(String.format("settings put system %s %s", "battery_level_enable", 1));
                // MT6580
                commands.add(String.format("settings put global %s %s", "multi_sim_data_call", 1));
            }

            // Generate new IMEI
            if (!Objects.equals(deviceId, "")) {
                AppUtil.updateIMEI(deviceId);
            }

            updateAPN(context);

            disableNotifications(context);
            restrictBackgroundDataUsage();
            cleanLauncherFavorites();
            cleanFiles();

            Preferences.from(context).put("setup", true).apply();

//                    commands.add("setenforce 0");
//                    commands.add("svc data enable");
//                    commands.add("svc wifi enable");
            commands.add("reboot");

            Shell.SU.run(commands);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // am broadcast -a magneticloop.action.UPDATE_FINISHED
    // (sleep 5 && am broadcast -a magneticloop.action.UPDATE_FINISHED)& && pm install -r /sdcard/Download/magneticloop.apk
    // pm install -r /sdcard/Download/magneticloop.apk;am broadcast -a magneticloop.action.UPDATE_FINISHED

    /**
     * @param path The path to apk file.
     */
    public static void update(final String path) {
        App.runInApplicationThread(new Runnable() {
            @Override
            public void run() {
                String installCmd = "pm install -r " + path;
                String broadcastCmd = "am broadcast -a " + Constants.ACTION_UPDATE_FINISHED;
//                String startCmd = "am start -n com.teslaamazing.magneticloop/.MainActivity";

                if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {

                    Shell.SU.run(installCmd);

//                    Shell.SU.run(new String[]{"(sleep 10 && " + startCmd + ")&", installCmd});
//                    Shell.SU.run("(sleep 7 && " + broadcastCmd + ")&");
//                    Shell.SU.run(installCmd);
//                    Shell.SU.run(new String[]{"(sleep 10 && " + startCmd + " && am start -n com.teslaamazing.magneticloop/.MainActivity) &", installCmd});
                } else {
                    Shell.SU.run(installCmd + " && " + broadcastCmd);
                }
            }
        });
    }
}
