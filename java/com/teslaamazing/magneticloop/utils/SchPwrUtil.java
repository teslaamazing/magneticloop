package com.teslaamazing.magneticloop.utils;

import com.teslaamazing.magneticloop.App;

import java.util.ArrayList;
import java.util.List;

import eu.chainfire.libsuperuser.Shell;

/**
 * Scheduled Power Util
 */
public class SchPwrUtil {

    public static final String URI = "content://com.mediatek.schpwronoff/schpwr";
    public static int TYPE_ON = 1;
    public static int TYPE_OFF = 2;

    /**
     * @param id
     * @param hour       from 1 to 24
     * @param minutes    from 0 to 60
     * @param daysOfWeek Bitmask format, from 0 to 127
     */
    public static void update(int id, int hour, int minutes, int daysOfWeek) {
        if (hour < 0) {
            hour = 0;
        } else if (hour > 24) {
            hour = 24;
        }
        if (minutes < 0) {
            minutes = 0;
        } else if (minutes > 60) {
            minutes = 60;
        }
        if (daysOfWeek < 0) {
            daysOfWeek = 0;
        } else if (daysOfWeek > 127) {
            daysOfWeek = 127;
        }

        App.log(String.format("Update schpwr: {id: %s, time: %s:%s, days: %s}", id, hour, minutes, daysOfWeek));

        List<String> commands = new ArrayList<>();

        commands.add(String.format("content delete --uri %s --where _id=%s", URI, id));
        commands.add(String.format("content insert --uri %s " +
                        "--bind _id:s:%d " +
                        "--bind hour:s:%d " +
                        "--bind minutes:s:%d " +
                        "--bind daysofweek:s:%d " +
                        "--bind enabled:s:%d",
                URI,
                id,
                hour,
                minutes,
                daysOfWeek,
                daysOfWeek > 0 ? 1 : 0
        ));

        Shell.SU.run(commands);
    }
}
