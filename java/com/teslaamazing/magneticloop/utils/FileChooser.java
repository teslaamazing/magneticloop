package com.teslaamazing.magneticloop.utils;

import java.io.File;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.text.DateFormat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.teslaamazing.magneticloop.R;

public class FileChooser extends ListActivity {

    private File currentDir;
    private FileArrayAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.file_explorer);

        Button closeButton = (Button) findViewById(R.id.bClose);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // initialize current directory
        currentDir = new File(Environment.getExternalStorageDirectory().toString());
        fill(currentDir);
    }

    private void fill(File file) {
        this.setTitle("Current Dir: " + file.getName());
        List<FileItem> dirs = new ArrayList<>();
        List<FileItem> files = new ArrayList<>();
        try {
            for (File f : file.listFiles()) {
                Date lastModDate = new Date(f.lastModified());
                DateFormat formater = DateFormat.getDateTimeInstance();
                String date_modify = formater.format(lastModDate);
                if (f.isDirectory()) {
                    File[] fbuf = f.listFiles();
                    int buf = fbuf != null ? fbuf.length : 0;
                    String num_item = String.valueOf(buf) + (buf == 0 ? " item" : " items");
                    dirs.add(new FileItem(f.getName(), num_item, date_modify, f.getAbsolutePath(), "directory_icon"));
                } else {
                    String ext = f.getName().substring(f.getName().indexOf(".") + 1);
                    String icon = "file_icon";
                    if (ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("3gp") || ext.equalsIgnoreCase("flv")) {
                        icon = "video_icon";
                    }
                    files.add(new FileItem(f.getName(), readableFileSize(f.length()), date_modify, f.getAbsolutePath(), icon));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Collections.sort(dirs);
        Collections.sort(files);
        dirs.addAll(files);

        if (!file.getName().equalsIgnoreCase("sdcard")) {
            dirs.add(0, new FileItem("..", "Parent Directory", "", file.getParent(), "directory_icon")); // directory_up
        }
        adapter = new FileArrayAdapter(FileChooser.this, dirs);
        this.setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);
        FileItem item = adapter.getItem(position);
        if (item.getImage().equalsIgnoreCase("directory_icon") || item.getImage().equalsIgnoreCase("directory_up")) {
            currentDir = new File(item.getPath());
            fill(currentDir);
        } else {
            onFileClick(item);
        }
    }

    /**
     * File click handler
     */
    private void onFileClick(FileItem item) {
        Intent intent = new Intent();
        intent.putExtra("path", item.getPath());
        intent.putExtra("name", item.getName());
        setResult(RESULT_OK, intent);
        finish();
    }

    public static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /**
     * Class FileArrayAdapter
     */
    public class FileArrayAdapter extends ArrayAdapter<FileItem> {

        private final Context context;
        private final int id;
        private final List<FileItem> items;

        public FileArrayAdapter(Context context, List<FileItem> items) {
            super(context, R.layout.file_view, items);
            this.context = context;
            this.id = R.layout.file_view;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = vi.inflate(id, null);
            }

         /* create a new view of my layout and inflate it in the row */
            //convertView = ( RelativeLayout ) inflater.inflate( resource, null );

            final FileItem item = items.get(position);
            if (item != null) {
                /* Take the ImageView from layout and set the item image */
                ImageView image = (ImageView) view.findViewById(R.id.file_icon);
                String name = "drawable/" + item.getImage();
                int resourceId = context.getResources().getIdentifier(name, null, context.getPackageName());
                Drawable imageDrawable = getResources().getDrawable(resourceId);
                image.setImageDrawable(imageDrawable);

                TextView fileName = (TextView) view.findViewById(R.id.file_name);
                TextView fileInfo = (TextView) view.findViewById(R.id.file_info);
                TextView fileDate = (TextView) view.findViewById(R.id.file_date);

                if (fileName != null) {
                    fileName.setText(item.getName());
                }
                if (fileInfo != null) {
                    fileInfo.setText(item.getData());
                }
                if (fileDate != null) {
                    fileDate.setText(item.getDate());
                }
            }
            return view;
        }

        public FileItem getItem(int i) {
            return items.get(i);
        }
    }
}
