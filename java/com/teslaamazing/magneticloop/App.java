package com.teslaamazing.magneticloop;

import android.content.Context;
import android.os.Handler;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.getsentry.raven.android.Raven;
import com.getsentry.raven.event.Event;
import com.getsentry.raven.event.EventBuilder;
import com.getsentry.raven.event.interfaces.ExceptionInterface;

public class App extends android.app.Application {

    public static boolean DEBUG = true; // BuildConfig.DEBUG;
    private static Handler mApplicationHandler = new Handler();

    @Override
    public void onCreate() {
        super.onCreate();

        initLogger();

        try {
            // workaround bug in AsyncTask, can show up (for example) when you toast from a service
            // this makes sure AsyncTask's internal handler is created from the right (main) thread
            Class.forName("android.os.AsyncTask");
        } catch (ClassNotFoundException e) {
            // will never happen
        }
    }

    public void initLogger() {
        Raven.init(this);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                logCapture(getApplicationContext(), paramThrowable);
                paramThrowable.printStackTrace();
                // System.exit(2); // Prevents the service/app from freezing
            }
        });
    }

    public void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()
                .penaltyLog()
                .build());

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .build());
    }

    /**
     * Run a runnable in the main application thread
     *
     * @param r The Runnable that will be executed.
     */
    public static boolean runInApplicationThread(Runnable r) {
        return mApplicationHandler.post(r);
    }

    /**
     * Run a runnable in the main application thread
     *
     * @param r           The Runnable that will be executed.
     * @param delayMillis The delay (in milliseconds) until the Runnable will be executed.
     */
    public static boolean runInApplicationThreadDelayed(Runnable r, long delayMillis) {
        return mApplicationHandler.postDelayed(r, delayMillis);
    }

    /**
     * Shows a toast message
     *
     * @param context Any context belonging to this application
     * @param message The message to show
     */
    public static void toast(Context context, String message) {
        // this is a static method so it is easier to call,
        // as the context checking and casting is done for you

        if (context == null) return;

        if (!(context instanceof App)) {
            context = context.getApplicationContext();
        }

        if (context instanceof App) {
            final Context c = context;
            final String m = message;

            runInApplicationThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(c, m, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    /**
     * @param msg The message you would like logged.
     * @return The number of bytes written.
     */
    public static int log(String msg) {
        return log(getCalledClass(4), msg, Log.DEBUG);
    }

    /**
     * @param msg      The message you would like logged.
     * @param priority The priority/type of this log message
     * @return The number of bytes written.
     */
    public static int log(String msg, int priority) {
        return log(getCalledClass(4), msg, priority);
    }

    /**
     * @param tag      tag Used to identify the source of a log message.
     * @param msg      The message you would like logged.
     * @param priority The priority/type of this log message
     * @return The number of bytes written.
     */
    public static int log(String tag, String msg, int priority) {
        if (DEBUG) {
            tag = String.format("Magnetic/%s", tag);
            if (tag.length() > 23) {
                tag = tag.substring(0, 23);
            }
            switch (priority) {
                case Log.VERBOSE:
                    return Log.v(tag, msg);
                case Log.DEBUG:
                    return Log.d(tag, msg);
                case Log.INFO:
                    return Log.i(tag, msg);
                case Log.WARN:
                    return Log.w(tag, msg);
                case Log.ERROR:
                    return Log.e(tag, msg);
            }
        }
        return 0;
    }

    public static void logCapture(Context context, String message) {
        logCapture(context, new EventBuilder()
                .withMessage(message)
                .withLevel(Event.Level.INFO)
        );
    }

    public static void logCapture(Context context, Throwable throwable) {
        logCapture(context, new EventBuilder()
                .withMessage(throwable.getMessage())
                .withLevel(Event.Level.ERROR)
                .withSentryInterface(new ExceptionInterface(throwable))
        );
    }

    private static void logCapture(Context context, EventBuilder eventBuilder) {
        TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        eventBuilder.withExtra("device_id", String.valueOf(telManager.getDeviceId()));
        eventBuilder.withExtra("sdk", String.valueOf(android.os.Build.VERSION.SDK_INT));
        Raven.capture(eventBuilder);
    }

    private static String getCalledClass(int index) {
        String className = Thread.currentThread().getStackTrace()[index].getClassName();
        className = className.substring(className.lastIndexOf('.') + 1);
        int pos = className.indexOf('$');
        if (pos > 0) {
            className = className.substring(0, pos);
        }
        return className;
    }
}
