package com.teslaamazing.magneticloop;

public class Constants {

    public static final String SERVER_URL = "https://teslaamazing.com/display/control";
    public static final String ROOT_APK_URL = "https://teslaamazing.com/media/apk/RootGenius-2.2.84_general_pc.apk";
    public static final String SETUP_FILES_URL = "https://teslaamazing.com/media/apk/files.zip";

    public static final String LOCKSETTINGS_DB = "/data/system/locksettings.db";
    public static final String TELEPHONY_CARRIERS_URI = "content://telephony/carriers";
    public static final String TELEPHONY_SIMINFO_URI = "content://telephony/siminfo";
    public static final String LAUNCHER_FAVORITES_URI = "content://com.android.launcher3.settings/favorites";
    public static final String RIL_CHANNEL = "/dev/radio/atci1";
    public static final String APN_NAME = "send.ee";
    public static final String APN_URL = "send.ee";

    public static final String UPDATE_FLAG = "update_flag";

    public static final int PING_INTERVAL = 5; // sec
    public static final int SERVER_INTERVAL = 30; // sec

//    public static final String TELEPHONY_DB = "/data/data/com.android.providers.telephony/databases/telephony.db";
//    public static final String LAUNCHER_DB = "/data/data/com.android.launcher3/databases/launcher.db";

    public static final String ACTION_USSD_CONNECTED = "magneticloop.action.USSD_CONNECTED";
    public static final String ACTION_USSD_REQUEST = "magneticloop.action.USSD_REQUEST";
    public static final String ACTION_USSD_RESPONSE = "magneticloop.action.USSD_RESPONSE";
    public static final String ACTION_UPDATE_FINISHED = "magneticloop.action.UPDATE_FINISHED";
    public static final String ACTION_ROOT_BEGIN = "magneticloop.action.ROOT_BEGIN";

    public static final String USSD_INFO_CODE = "146*099";
    public static final String USSD_RESPONSE_PATTERN = "([0-9]{11}) Balance: is (.+) [A-Z]{3}";
}

// adb shell am broadcast -a magneticloop.action.USSD_REQUEST -e code 146*099
// adb shell am start -n com.teslaamazing.magneticloop/.MainActivity
// adb shell am force-stop com.teslaamazing.magneticloop
// adb uninstall com.teslaamazing.magneticloop

// content query --uri content://telephony/siminfo
// content update --uri content://telephony/siminfo --bind data_roaming:s:1
// content update --uri content://settings/secure --bind package_verifier_enable:s:0

// content query --uri content://telephony/carriers
// content query --uri content://telephony/carriers/preferapn
// content query --uri content://com.mediatek.schpwronoff/schpwr
// content query --uri content://settings/system
// content query --uri content://settings/global
// content query --uri content://settings/secure

// content insert --uri content://telephony/carriers/preferapn --bind apn_id:i:9999

// content delete --uri content://com.mediatek.schpwronoff/schpwr --where _id=1
// content insert --uri content://com.mediatek.schpwronoff/schpwr --bind hour:s:1 --bind minutes:s:2 --bind _id:s:1
// content insert --uri content://com.mediatek.schpwronoff/schpwr --bind _id:s:1 --bind hour:s:1 --bind minutes:s:2 --bind daysofweek:s:127 --bind enabled:s:1