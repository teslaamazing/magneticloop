package com.teslaamazing.magneticloop;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class USSDService extends AccessibilityService {

    private static boolean connected = false;

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

        List<CharSequence> eventText;

        App.log("EVENT: " + event.toString());

        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                // com.mediatek.phone.UssdAlertActivity;
                if (!String.valueOf(event.getClassName()).contains("AlertDialog")) {
                    return;
                }
                eventText = event.getText();
                break;
            case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                if (!event.getClassName().equals("android.widget.TextView")) {
                    return;
                }
                AccessibilityNodeInfo source = event.getSource();
                if (source == null || TextUtils.isEmpty(source.getText())) {
                    return;
                }
                eventText = Collections.singletonList(source.getText());
                break;
            default:
                return;
        }

        App.log("RESPONSE: " + eventText.toString());

        Response res = new Response(eventText);
        if (TextUtils.isEmpty(res.phone)) return;

        // Close dialog
        performGlobalAction(GLOBAL_ACTION_BACK); // This works on 4.1+ only

        Intent intent = new Intent();
        intent.setAction(Constants.ACTION_USSD_RESPONSE);
        intent.putExtra("response", res);
        sendBroadcast(intent);
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.flags = AccessibilityServiceInfo.DEFAULT;
        info.packageNames = new String[]{"com.android.phone"};
//        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED | AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        info.notificationTimeout = 0;
        setServiceInfo(info);

        Intent intent = new Intent();
        intent.setAction(Constants.ACTION_USSD_CONNECTED);
        intent.putExtra("info", info);
        sendBroadcast(intent);

        connected = true;

//        App.log("USSDService - connected");
    }

    /**
     * class Response
     */
    public static class Response implements Serializable {
        public String phone;
        public Float balance;

        public Response(String text) {
            process(text);
        }

        public Response(List<CharSequence> eventText) {
            for (CharSequence line : eventText) {
                process(String.valueOf(line));
            }
        }

        private void process(String text) {
            if (!TextUtils.isEmpty(text)) {
                Pattern pattern = Pattern.compile(Constants.USSD_RESPONSE_PATTERN);
                Matcher matcher = pattern.matcher(text);
                if (matcher.matches()) {
                    phone = matcher.group(1);
                    balance = Float.valueOf(matcher.group(2));
                }
            }
        }
    }

    /**
     * @param context Context
     * @param code    Without start symbol(*) and end symbol (#)
     */
    public static void request(Context context, String code) {
        if (connected) {
            context.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:*" + code + Uri.encode("#"))));
        } else {
            App.log("USSDService is not connected");
        }
    }
}