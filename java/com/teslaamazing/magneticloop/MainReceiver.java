package com.teslaamazing.magneticloop;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import java.io.File;

public class MainReceiver extends BroadcastReceiver {

    private void openMainActivity(Context context) {
        Intent activity = new Intent(context, MainActivity.class);
        activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(activity);
    }

    public void clearUpdateCache() {
        App.log("Clear update cache...");
        File apk = new File(Environment.getExternalStorageDirectory().toString() + "/Download/magneticloop.apk");
        if (apk.exists()) {
            apk.delete();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        App.log("Receive action: " + intent.getAction());

        Preferences prefs = Preferences.from(context);

        if (intent.getAction() != null) {
            switch (intent.getAction()) {
                case Intent.ACTION_BOOT_COMPLETED:
                    openMainActivity(context);
                    break;
                case Constants.ACTION_USSD_CONNECTED:
                    if (prefs.getBoolean(Constants.UPDATE_FLAG)) {
                        prefs.put(Constants.UPDATE_FLAG, null);
                        clearUpdateCache();
                    }
                    openMainActivity(context);
                    break;
                case Constants.ACTION_UPDATE_FINISHED:
                    clearUpdateCache();
                    openMainActivity(context);
                    break;
            }
        }
    }

}
