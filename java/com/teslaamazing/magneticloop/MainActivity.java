package com.teslaamazing.magneticloop;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.TextView;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import eu.chainfire.libsuperuser.Shell;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.Callback;
import com.afollestad.bridge.Form;
import com.afollestad.bridge.Request;
import com.afollestad.bridge.Response;
import com.teslaamazing.magneticloop.task.PingTask;
import com.teslaamazing.magneticloop.task.RequestTask;
import com.teslaamazing.magneticloop.utils.AppUtil;
import com.teslaamazing.magneticloop.utils.GPSTracker;
import com.teslaamazing.magneticloop.utils.SchPwrUtil;
import com.teslaamazing.magneticloop.utils.WakeLocker;

import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {

    private TextView networkStatusTextView;
    private SimpleExoPlayerView simpleExoPlayerView;

    @Nullable
    private SimpleExoPlayer player;
    private ConnectivityManager conManager;
    private TelephonyManager telManager;
    private WifiManager wifiManager;
    private Preferences prefs;

    @Nullable
    private ProgressDialog pDialog;

    private Handler mainHandler = new Handler();
    private ScheduledExecutorService scheduleTaskExecutor;
    private DataSource.Factory mediaDataSourceFactory;
    private TrackSelection.Factory videoTrackSelectionFactory;
    private DefaultTrackSelector trackSelector;
    private LoadControl loadControl;
    private static final DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

    private int touchCounter;
    private boolean suAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        if (App.DEBUG) {
            ((App) getApplicationContext()).enableStrictMode();
        }

        networkStatusTextView = (TextView) findViewById(R.id.network_status);

        // Initialize player
        simpleExoPlayerView = (SimpleExoPlayerView) findViewById(R.id.player_view);
        simpleExoPlayerView.setVisibility(View.INVISIBLE);
        simpleExoPlayerView.requestFocus();
        simpleExoPlayerView.setUseController(false);
        simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        mediaDataSourceFactory = buildDataSourceFactory(true);

        conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        telManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        suAvailable = Shell.SU.available();
        prefs = Preferences.from(this);

        scheduleTaskExecutor = Executors.newScheduledThreadPool(5);

        View mContentView = findViewById(R.id.content);
        mContentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    touchCounter++;
                }
                if (touchCounter >= 10) {
                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivityForResult(intent, 1);
                    touchCounter = 0;
                }
                return true;
            }
        });

        // Network change state broadcast
        IntentFilter netFilter = new IntentFilter();
        netFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
//        netFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
//        netFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
//        netFilter.addAction(Constants.ACTION_USSD_CONNECTED);
        netFilter.addAction(Constants.ACTION_USSD_RESPONSE);
        netFilter.addAction(Constants.ACTION_USSD_REQUEST);
        registerReceiver(new Receiver(), netFilter);

        checkAccessability();

        // parse phone number and balance
        if (TextUtils.isEmpty(prefs.getString(Preferences.Key.PHONE))) {
            if (prefs.getBoolean("setup") && isMobileAvailable()) {
                USSDService.request(this, Constants.USSD_INFO_CODE);
            }
        }

        // prepare wifi settings
        wifiManager.setWifiEnabled(true);
        AppUtil.configureWifi(wifiManager);

        // TODO: optimize
        if (suAvailable) {
            List<String> commands = new ArrayList<>();
            commands.add("setenforce 0");
            commands.add("svc data enable");
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                commands.add(String.format("settings put secure %s %s", "immersive_mode_confirmations", "confirmed"));
            }
            Shell.SU.run(commands);
        }
    }

    /**
     * class Receiver
     */
    private class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {

                case Constants.ACTION_USSD_CONNECTED:
                    if (TextUtils.isEmpty(prefs.getString(Preferences.Key.PHONE))) {
                        if (isMobileAvailable()) {
                            USSDService.request(context, Constants.USSD_INFO_CODE);
                        }
                    }
                    break;

                case Constants.ACTION_USSD_REQUEST:
                    String code = intent.getStringExtra("code");
                    if (!TextUtils.isEmpty(code)) {
                        USSDService.request(context, code);
                    }
                    break;

                case Constants.ACTION_USSD_RESPONSE:
                    USSDService.Response res = (USSDService.Response) intent.getSerializableExtra("response");
                    if (!TextUtils.isEmpty(res.phone)) {
                        prefs.put(Preferences.Key.PHONE, res.phone);
                        prefs.put(Preferences.Key.BALANCE, res.balance);
                        prefs.apply();
                    }
                    break;

                case ConnectivityManager.CONNECTIVITY_ACTION:
                    NetworkInfo activeNetwork = conManager.getActiveNetworkInfo();
                    String msg;
                    if (activeNetwork != null) {
                        msg = String.format("Network - %s (%s: %s)",
                                activeNetwork.getDetailedState(),
                                activeNetwork.getTypeName(),
                                activeNetwork.getExtraInfo()
                        );
                        if (activeNetwork.isConnectedOrConnecting()) {
                            startNetworkChecking();
                        } else {
                            msg = "Network - DISCONNECTED";
                            stopServerTracking();
                        }
                    } else {
                        msg = "Network - INACTIVE";
                        stopServerTracking();
                    }
                    networkStatusTextView.setText(msg);
                    App.toast(MainActivity.this, msg);
                    App.log(msg);
                    break;
            }
        }
    }

    /**
     * Applies the correct flags to the windows decor view to enter
     * or exit fullscreen mode
     *
     * @param fullscreen True if entering fullscreen mode
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void setUiFlags(boolean fullscreen) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            View decorView = getWindow().getDecorView();
            if (decorView != null) {
                decorView.setSystemUiVisibility(fullscreen ? getFullscreenUiFlags() : View.SYSTEM_UI_FLAG_VISIBLE);
            }
        }
    }

    /**
     * Determines the appropriate fullscreen flags based on the
     * systems API version.
     *
     * @return The appropriate decor view flags to enter fullscreen mode when supported
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private int getFullscreenUiFlags() {
        int flags = View.SYSTEM_UI_FLAG_LOW_PROFILE | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT && suAvailable) {
            flags |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        return flags;
    }

    private void goFullscreen() {
        setUiFlags(true);
    }

    private void exitFullscreen() {
        setUiFlags(false);
    }

    @Override
    public void onNewIntent(Intent intent) {
        releasePlayer();
        setIntent(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        goFullscreen();
        showInfoToast();
        if (Util.SDK_INT > 23) {
            initializePlayer();
            WakeLocker.acquire(getApplicationContext());
//        receiver.setNextAlarm(getApplicationContext(), true, 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        goFullscreen();
        if (Util.SDK_INT <= 23) {
            initializePlayer();
            WakeLocker.acquire(getApplicationContext());
//        receiver.setNextAlarm(getApplicationContext(), true, 0);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
            WakeLocker.release();
//        receiver.setNextAlarm(getApplicationContext(), false, 1);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
            WakeLocker.release();
//        receiver.setNextAlarm(getApplicationContext(), false, 1);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.isLongPress() && event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN) {
            Shell.SU.run("svc power shutdown");
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        exitFullscreen();
        scheduleTaskExecutor.shutdown();
    }

    protected void initializePlayer() {
        if (player == null) {
            videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
            trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            loadControl = new DefaultLoadControl();

            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
            player.setPlayWhenReady(true);
            player.addListener(new ExoPlayer.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                    MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                    if (mappedTrackInfo != null) {
                        if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO)
                                == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                            App.log(getString(R.string.error_unsupported_video));
                        }
                        if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_AUDIO)
                                == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                            App.log(getString(R.string.error_unsupported_audio));
                        }
                    }
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == ExoPlayer.STATE_ENDED) {
                        prefs.incrementStat();
                        player.seekTo(0);
                    }
                }

                @Override
                public void onPlayerError(ExoPlaybackException e) {
                    String errorString = null;
                    if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                        Exception cause = e.getRendererException();
                        if (cause instanceof DecoderInitializationException) {
                            // Special case for decoder initialization failures.
                            DecoderInitializationException decoderInitializationException =
                                    (DecoderInitializationException) cause;
                            if (decoderInitializationException.decoderName == null) {
                                if (decoderInitializationException.getCause() instanceof DecoderQueryException) {
                                    errorString = getString(R.string.error_querying_decoders);
                                } else if (decoderInitializationException.secureDecoderRequired) {
                                    errorString = getString(R.string.error_no_secure_decoder,
                                            decoderInitializationException.mimeType);
                                } else {
                                    errorString = getString(R.string.error_no_decoder,
                                            decoderInitializationException.mimeType);
                                }
                            } else {
                                errorString = getString(R.string.error_instantiating_decoder,
                                        decoderInitializationException.decoderName);
                            }
                        }
                    }

                    if (errorString != null) {
                        App.log(errorString);
//                        App.toast(getApplicationContext(), errorString);
                    }

                    App.logCapture(getApplicationContext(), e);
                }

                @Override
                public void onPositionDiscontinuity() {

                }
            });

            simpleExoPlayerView.setPlayer(player);
//            player.prepare(buildMediaSource(Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")), true, false);

            String video = prefs.getVideoFile();
            if (!TextUtils.isEmpty(video)) {
                File videoFile = new File(video);
                if (videoFile.exists()) {
                    simpleExoPlayerView.setVisibility(View.VISIBLE);
                    player.prepare(buildMediaSource(Uri.fromFile(videoFile)), true, false);
                } else {
                    App.log(String.format("Video \"%s\" not exists.", video));
                }
            }
        }
    }

    protected MediaSource buildMediaSource(Uri uri) {
        int type = Util.inferContentType(uri.getLastPathSegment());
        switch (type) {
            case C.TYPE_SS:
                return new SsMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, null);
            case C.TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, null);
            case C.TYPE_HLS:
                return new HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, null);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource(uri, mediaDataSourceFactory, new DefaultExtractorsFactory(),
                        mainHandler, null);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    protected void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set {@link #bandwidthMeter} as a listener to the new
     *                          DataSource factory.
     * @return A new DataSource factory.
     */
    protected DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        return buildDataSourceFactory(useBandwidthMeter ? bandwidthMeter : null);
    }

    protected DataSource.Factory buildDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultDataSourceFactory(this, bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter));
    }

    protected HttpDataSource.Factory buildHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultHttpDataSourceFactory(Util.getUserAgent(this, "Magneticloop"), bandwidthMeter);
    }

    protected void checkAccessability() {
        if (suAvailable && !AppUtil.isAccessibilitySettingsOn(this)) {
            App.log("enable accessability service");
            AppUtil.enableAccessabilityService(this, getPackageName() + "/" + USSDService.class.getCanonicalName());
        }
    }

    protected Boolean isMobileAvailable() {
        return telManager.getNetworkOperator() != null && !telManager.getNetworkOperator().equals("");
    }

    @Nullable
    protected ScheduledFuture pingScheduledFuture;

    @Nullable
    protected ScheduledFuture requestScheduledFuture;

    @UiThread
    protected void startNetworkChecking() {
        if (pingScheduledFuture != null) {
            return;
        }
        pingScheduledFuture = scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                new PingTask(new PingTask.EventListener() {
                    @Override
                    public void onPostExecute(boolean online) {
                        App.log("ping - " + (online ? "online" : "offline"));
                        if (online) {
                            if (pingScheduledFuture != null) {
                                pingScheduledFuture.cancel(false);
                                pingScheduledFuture = null;
                                // mobile data test is enabled
                                if (prefs.getBoolean(Preferences.Key.TEST_MOBILE)) {
                                    wifiManager.setWifiEnabled(true);
                                    prefs.put(Preferences.Key.TEST_MOBILE, null);
                                    startNetworkChecking();
                                } else {
                                    if (suAvailable) {
                                        startServerTracking();
                                    } else {
                                        if (!isFinishing()) {
                                            AppUtil.root(MainActivity.this);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }).execute();
            }
        }, 10, Constants.PING_INTERVAL, TimeUnit.SECONDS);
    }

    protected void startServerTracking() {
        if (requestScheduledFuture != null) {
            return;
        }
        App.log("start server tracking");
        requestScheduledFuture = scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {

                final NetworkInfo activeNetwork = conManager.getActiveNetworkInfo();

                new RequestTask(new RequestTask.EventListener() {

                    @Override
                    public void onPreExecute(Form form) {

                        String deviceId = telManager.getDeviceId();
                        if (TextUtils.isEmpty(deviceId)) {
                            deviceId = "000000000000000";
                        }
                        form.add("id", deviceId);
                        form.add("stat", prefs.getStat());

                        String phone = prefs.getString(Preferences.Key.PHONE, telManager.getLine1Number());
                        if (!TextUtils.isEmpty(phone)) {
                            form.add("phone", phone);
                        }

                        GPSTracker gps = new GPSTracker(getApplicationContext());
                        if (gps.canGetLocation()) {
                            form.add("lat", gps.getLatitude());
                            form.add("lng", gps.getLongitude());
                        }

                        if (TextUtils.isEmpty(prefs.getVideoFile())) {
                            form.add("video", "-1");
                        }

                        if (activeNetwork != null) {
                            form.add("nt", activeNetwork.getType());
                        }

                        if (!TextUtils.isEmpty(prefs.getString("cmd_response"))) {
                            form.add("cmd_response", prefs.getString("cmd_response"));
                            prefs.put("cmd_response", null);
                        }

                        try {
                            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                            form.add("v", pInfo.versionName);
                            form.add("hash", AppUtil.makeSHA1Hash(pInfo.packageName + "#" + pInfo.versionName));
                        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onPostExecute(JSONObject result) throws JSONException {

                        boolean isWifiConnected = activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

                        // setup new device
                        if (result.has("device_id") && isWifiConnected) {
                            setupApp(result.getString("device_id"));
                        } else {

                            // run custom command
                            if (result.has("command")) {
                                String stdout = TextUtils.join("\n", Shell.SU.run(result.getString("command")));
                                prefs.put("cmd_response", stdout);
                                App.logCapture(getApplicationContext(), stdout);
                            }

                            // update application
                            if (result.has("update")) {
                                JSONObject update = result.getJSONObject("update");
                                if (update.has("url")) {
                                    updateApp(update.getString("url"));
                                }
                            }

                            // update video
                            if (result.has("video")) {
                                if (prefs.getBoolean("setup")) {
                                    if (isWifiConnected) {
                                        updateVideo(result.getString("video"));
                                    }
                                } else {
                                    updateVideo(result.getString("video"));
                                }
                            }

                            // enable mobile data test
                            if (result.has(Preferences.Key.TEST_MOBILE)) {
                                prefs.put(Preferences.Key.TEST_MOBILE, true);
                                wifiManager.setWifiEnabled(false);
                            }

                            // scheduled power
                            JSONObject schpwr;
                            if (result.has("schpwr_on")) {
                                schpwr = result.getJSONObject("schpwr_on");
                                if (schpwr.has("hour") && schpwr.has("minutes") && schpwr.has("days")) {
                                    SchPwrUtil.update(SchPwrUtil.TYPE_ON, schpwr.getInt("hour"), schpwr.getInt("minutes"), schpwr.getInt("days"));
                                }
                            }
                            if (result.has("schpwr_off")) {
                                schpwr = result.getJSONObject("schpwr_off");
                                if (schpwr.has("hour") && schpwr.has("minutes") && schpwr.has("days")) {
                                    SchPwrUtil.update(SchPwrUtil.TYPE_OFF, schpwr.getInt("hour"), schpwr.getInt("minutes"), schpwr.getInt("days"));
                                }
                            }

                            // enable or disable wifi
                            if (result.has("wifi")) {
                                wifiManager.setWifiEnabled(result.getBoolean("wifi"));
                            }

                            // update preferences
                            if (result.has("prefs")) {
                                prefs.sync(result.getJSONObject("prefs"));
                            }
                        }
                    }

                }).execute(prefs.getServerUrl());

                // TODO: remove
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            goFullscreen();
                        }
                    });
                }

            }
        }, 10, prefs.getServerInterval(), TimeUnit.SECONDS);
    }

    protected void stopServerTracking() {
        if (requestScheduledFuture != null) {
            App.log("stop server tracking");
            requestScheduledFuture.cancel(false);
            requestScheduledFuture = null;
        }
    }

    /**
     * @param url The url to video file.
     */
    @UiThread
    protected void updateVideo(String url) {
        if (isFinishing() || pDialog != null && pDialog.isShowing()) {
            return;
        }
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setMax(100);
        pDialog.setTitle("New Video Available");
        pDialog.setMessage("Downloading...");
        pDialog.show();

        String tag = "new_video";
        String path = Environment.getExternalStorageDirectory().toString() + "/Download";
//        final File file = new File(path, "default.mp4");
        final File file = new File(path, URLUtil.guessFileName(url, null, null));

        Bridge.cancelAll().tag(tag).commit();
        Bridge.get(url).tag(tag).request(new Callback() {
            @Override
            public void response(@NonNull Request request, @Nullable Response response, @Nullable BridgeException e) {
                try {
                    if (response != null) {
                        response.asFile(file);
                        if (file.exists()) {
                            prefs.put(Preferences.Key.VIDEO_FILE, file.getAbsolutePath()).apply();
                            if (player != null) {
                                player.prepare(buildMediaSource(Uri.fromFile(file)), true, false);
                                simpleExoPlayerView.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    pDialog.dismiss();
                    goFullscreen();
                } catch (BridgeException e1) {
                    e1.printStackTrace();
                    pDialog.dismiss();
                }
            }

            @Override
            public void progress(Request request, int current, int total, int percent) {
                super.progress(request, current, total, percent);
                pDialog.setProgress(percent);
            }
        });
    }

    /**
     * @param url The url to apk file.
     */
    @UiThread
    protected void updateApp(String url) {

        if (isFinishing() || pDialog != null && pDialog.isShowing()) {
            return;
        }

        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setMax(100);
        pDialog.setTitle("New App Available");
        pDialog.setMessage("Downloading...");
        pDialog.show();

        String tag = "new_app";
        final String path = Environment.getExternalStorageDirectory().toString() + "/Download";

        stopServerTracking();

        Bridge.cancelAll().tag(tag).commit();
        Bridge.get(url).tag(tag).request(new Callback() {
            @Override
            public void response(@NonNull Request request, @Nullable Response response, @Nullable BridgeException e) {
                final File file = new File(path, "magneticloop.apk");
                try {
                    if (response != null) {
                        prefs.put(Constants.UPDATE_FLAG, true).apply();
                        response.asFile(file);
                        AppUtil.update(file.getAbsolutePath());
                    }
                } catch (BridgeException e1) {
                    App.logCapture(getApplicationContext(), e1);
                    e1.printStackTrace();
                    startServerTracking();
                } finally {
                    pDialog.dismiss();
                }
            }

            @Override
            public void progress(Request request, int current, int total, int percent) {
                super.progress(request, current, total, percent);
                pDialog.setProgress(percent);
            }
        });
    }

    /**
     * @param deviceId
     */
    @UiThread
    protected void setupApp(final String deviceId) {

        stopServerTracking();

        if (isFinishing() || pDialog != null && pDialog.isShowing()) {
            return;
        }

        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setTitle("Magnetic Loop");
        pDialog.setMessage("Installation... Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.setMax(100);
        pDialog.show();

        final String path = Environment.getExternalStorageDirectory().toString() + "/Download/files.zip";
        final String tag = "setup";

        Bridge.cancelAll().tag(tag).commit();
        Bridge.get(Constants.SETUP_FILES_URL).tag(tag).retries(5, 2000).request(new Callback() {
            @Override
            public void response(@NonNull Request request, @Nullable Response response, @Nullable BridgeException e) {
                File file = new File(path);
                try {
                    if (response != null) {
                        response.asFile(file);
                        AppUtil.setup(MainActivity.this, file, deviceId);
                    }
                } catch (BridgeException e1) {
                    e1.printStackTrace();
                    startServerTracking();
                } finally {
                    pDialog.hide();
                }
            }

            @Override
            public void progress(Request request, int current, int total, int percent) {
                super.progress(request, current, total, percent);
                pDialog.setProgress(percent);
            }
        });
    }

    /**
     * Show info box
     */
    protected void showInfoToast() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String phoneNumber = prefs.getString(Preferences.Key.PHONE, telManager.getLine1Number());
            String deviceId = telManager.getDeviceId();
            if (deviceId != null && deviceId.length() == 15) {
                deviceId = deviceId.substring(0, 11) + "  " + deviceId.substring(11, 15);
            }
            App.toast(getApplicationContext(), String.format("Version: %s\n\nID: %s\nPhone: %s\nRoot: %s",
                    pInfo.versionName,
                    deviceId,
                    phoneNumber,
                    suAvailable ? "ok" : "no"
            ));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
